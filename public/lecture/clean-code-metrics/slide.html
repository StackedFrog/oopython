<!doctype html>
<html class="theme-5">
<meta charset="utf-8" />
<link href="../html-slideshow.bundle.min.css" rel="stylesheet" />
<link href="../style.css" rel="stylesheet" />
<script src="https://dbwebb.se/cdn/js/html-slideshow_v1.1.0.bundle.min.js"></script>

<title>Cleaner code</title>

<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Cleaner code
## Attributes of clean code and using metrics to help
### Mikael Roos
</script>



<script data-role="slide" data-markdown type="text/html">
# Agenda

* Attributes and design philosophies of clean code
* C:s to improve clean code
* Software metrics to maintain code

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# C:s to improve clean code
</script>



<script data-role="slide" data-markdown type="text/html">
# C:s improving clean code

* Coding conventions
* Code smell
* Cohesion
* Coupling
* Code coverage
* Cyclomatic complexity

</script>



<script data-role="slide" data-markdown type="text/html">
# Coding conventions

* Follow a recommended coding style convention all through your project
* Use tools to enforce it
    * Tool to check if the style is correct
    * Tool to fix the style

<p class="footnote">https://en.wikipedia.org/wiki/Coding_conventions</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Code smell

* Some mess in the code that might indicate a deeper problem
* Issues that do not follow best practice
* Static code analysis to find issues and potential problems
    * Mess detectors

<p class="footnote">https://en.wikipedia.org/wiki/Code_smell</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Code coverage

* Add unit tests for your code
* Measure how many lines of code is covered by the tests
* More test coverage may imply that
    * It is easier to maintain the software
    * The software has a reasonable quality level

<p class="footnote">https://en.wikipedia.org/wiki/Code_coverage</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Cohesion

* Degree to which the elements inside a module belong together
* A cohesive class performs one feature while a non-cohesive class performs two or more features.

> "The ideal value of this metric is 1 (high cohesion) meaning a class has only one responsibility (good) and value X means that a class has probably X responsibilities (bad)."

* LCOM (Lack of Cohesive Methods)

<p class="footnote">https://en.wikipedia.org/wiki/Cohesion_(computer_science)</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# LCOM

<figure>
<img src="img/lcom4.png" width="60%">
<figcaption>Dependency graph on the class relationships providing the value for Lack of Cohesive Methods (LCOM).</figcaption>
</figure>

<p class="footnote">https://softwareengineering.stackexchange.com/questions/151004/are-there-metrics-for-cohesion-and-coupling</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Coupling

Degree of interdependence between software modules; a measure of how closely connected two classes are.

* The strength of the relationships between classes
* Metrics for
    * Incoming coupling
    * Outgoing coupling

> "Generally, loose coupled classes and components are preferable as the higher your coupling, the higher are the chances that a class breaks or requires adaption because of changes in classes that it depends on."

<p class="footnote">https://en.wikipedia.org/wiki/Coupling_(computer_programming)<br>https://en.wikipedia.org/wiki/Loose_coupling</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Low coupling, high cohesion

> "**Low coupling** is often a sign of a well-structured computer system and a good design, and when combined with<br>**high cohesion** it supports the general goals of high readability and maintainability."

</script>



<script data-role="slide" data-markdown type="text/html">
# Cyclomatic complexity

* Cyclomatic complexity is a software metric used to indicate the complexity of a program
* Quantitative measure of the number of linearly independent paths through a program's source code
* If the source code contains no control flow statements (conditionals or decision points), the complexity would be 1

> "Avoid tabbing in your code."

<p class="footnote">https://en.wikipedia.org/wiki/Cyclomatic_complexity</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Complexity by graph

<figure>
<img style="background-color: white; padding: 1em;" src="img/complexity-graph.png" width="80%">
<figcaption>Cyclomatic complexity is computed using the control flow graph of the program: the nodes of the graph correspond to indivisible groups of commands of a program, and a directed edge connects two nodes if the second command might be executed immediately after the first command. CC=3</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# CC = 1

```
1   function example() {
        fiddle();
        fiddle();
        fiddle();
        return true;
    }
```

<p class="footnote">No decision points. Cyclomatic complexity = 1 + 0.</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# CC = 6

```
1   function example() {
2       if ($a == $b) {
3           if ($a1 == $b1) {
                fiddle();
4           } elseif ($a2 == $b2) {
                fiddle();
            } else {
                fiddle();
            }
5       } elseif ($c == $d) {
6           while ($c == $d) {
                fiddle();
            }
        } else {
```

<p class="footnote">6 decision points. Cyclomatic complexity = 1+5.</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# CC guideline

> "Cyclomatic Complexity of a method should not exceed 10."

> "Split into smaller method whenever the cyclomatic complexity of the module exceeded 10."

> "For each method, either limit cyclomatic complexity to [the agreed-upon limit] or provide a written explanation of why the limit was exceeded."

* High CC makes code harder to test (and maintain)

</script>



<script data-role="slide" data-markdown type="text/html">
# C:s improving clean code

* Coding conventions
* Code smell
* Cohesion
* Coupling
* Code coverage
* Cyclomatic complexity

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Software metrics
## For maintaining code
</script>



<script data-role="slide" data-markdown type="text/html">
# Maintainability rating

Various measures to try and estimate effort, cost and risk of maintaining and developing code.

* Quality of code
* Estimated time to maintain of develop it
* Complexity of the code
* Risk the code introduces
* Technical depth

</script>



<script data-role="slide" data-markdown type="text/html">
# Maintainability index

The Maintainability Index tries to give a hint of the relative maintenance burden for different sections of a project by blending together a series of different metrics.

* Size and cyclomatic complexity

```
Maintainability Index = MAX(
        0, (
            171 - 5.2 * ln(Halstead Volume)
            - 0.23 * (Cyclomatic Complexity)
            - 16.2 * ln(Lines of Code)
        ) *100 / 171)
```

* 0-9 = Red
* 10-19 = Yellow
* 20-100 = Green

<p class="footnote">Used by Microsoft<br>https://learn.microsoft.com/en-us/visualstudio/code-quality/code-metrics-maintainability-index-range-and-meaning</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# SQALE method

Maintainability rating through "Software Quality Assessment based on Lifecycle Expectations" (SQALE). The rating given to your project related to the value of your Technical debt ratio.

* What is the quality of the source code delivered by the developers?
* Is the code changeable, maintainable, portable, reusable?
* What is the design debt stored up by the project?
    * Using technical debt ratio
* Measure of effort to fix all code smells

* Used by SonarCube software analyse system (for example)

<p class="footnote">https://en.wikipedia.org/wiki/SQALE</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# CRAP index

CRAP is short for Change Risk Anti-Patterns - a mildly offensive acronym to protect you from deeply offensive code.

* Metric comparing cyclomatic complexity with code coverage
    * CC 10 -> 43%
    * CC 25 -> 80%
    * CC 30 -> no tests will help you here

* Addresses the risk of changing a bit of code
* Methods with high CRAP scores are risky to change
* It is all about assessing risk

<!--
If you write a method with a cyclomatic complexity of 10, you bring it below the CRAP threshold of 30 by covering 42% or more of it with tests.  With a cyclomatic complexity of 25, you’ll need to get that coverage up to 80%.  But if your cyclomatic complexity exceeds 30, then no amount of testing in the world can make that method non-CRAP-y.
-->

</script>



<script data-role="slide" data-markdown type="text/html">
# Maintainability rating

* (Microsoft) Maintainability index
    * Size and cyclomatic complexity
* (Sonarcube) Maintainability rating (SQALE)
    * Using technical debt ratio
* CRAP index
    * Cyclomatic complexity and unit tests

</script>



<script data-role="slide" data-markdown type="text/html">
# Good code?

> "Good and clean code should be readable and easy to maintain."

<p class="footnote">What will your own definition be of clean code?</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Summary

* Attributes and design philosophies of clean code
* C:s to improve clean code
* Software metrics to maintain code

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# The End
</script>



<script data-role="slide" data-markdown type="text/html">
</script>
