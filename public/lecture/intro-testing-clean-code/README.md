---
author: mos
revision: 
    2024-02-13: "(B, mos) Minor update to structure."
    2023-01-03: "(A, mos) First version."
---
Introduction to testing and clean code
====================

This is an introduction to unit testing and the concept of clean code.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/oopython/lecture/intro-testing-clean-code/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

[![slide](img/slide.png)](https://mikael-roos.gitlab.io/oopython/lecture/intro-testing-clean-code/slide.html)

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheuq/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheuq)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. Review a few of [the most common software philosophies](https://en.wikipedia.org/wiki/List_of_software_development_philosophies).

<!--
Some basic reading instruction on clean code?
-->
