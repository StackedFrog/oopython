---
author: mos
revision: 
    2023-01-03: "(A, mos) first version"
---
Cleaner code - TDD, documentation and cleaner code
====================

The presentation provides the following:

* Introduction to Test-driven Development,
* shows various ways of working with automated documentation,
* and talks about some aspects of how to get cleaner code.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/oopython/lecture/tdd-documentation/slide.html) (press f/esc to enter/exit full-screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

<!--
1. The book "Microsoft Visual C# Step by Step, 10th Edition".

    * CHAPTER 1 - Welcome to C#
    * CHAPTER 2 - Working with variables, operators, and expressions
-->
