I have reviewed your report through the following steps.

I do check that it matches the general requirements from the specifikation:
https://hkr.instructure.com/courses/3641/pages/assignment-3-sustainable-programming-through-good-and-clean-code

I have graded your report.

Here are a summary of my general comments made to all of you.
https://hkr.instructure.com/courses/3641/discussion_topics/252959

Let me know if you have any questions.

//Mikael



Tacksam för din kursfeedback!

//Mikael

Thanks for your course feedback!

//Mikael
